package com.example.teste_ml_kit;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;

import com.google.android.gms.analytics.*;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.*;


public class MainActivity extends AppCompatActivity {

    Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deepLink);
        tracker = GoogleAnalytics.getInstance(this).newTracker("UA-00000000");

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            Log.w("DEEPLINK", deepLink.getEncodedQuery());
                            HitBuilders.ScreenViewBuilder screen = new HitBuilders.ScreenViewBuilder();



                            screen.setCampaignParamsFromUrl(deepLink.toString());

                            if((screen.build().get("&cs") == null) && (deepLink.toString().length() != 0)){
                                screen.set("&cs", deepLink.getHost());//Enviar aqui apenas o host no exemplo https://www.natura.com.br
                                screen.set("&cm", "referrer");
                            };
                            tracker.send(screen.build());
                        } else {
                            HitBuilders.ScreenViewBuilder screen = new HitBuilders.ScreenViewBuilder();
                            tracker.send(screen.build());
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }

}
